package coms.example.arqam.foodhub;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

/**
 * Created by XPS on 12/28/2017.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {

    private List<Categories> categoriesList;
    private Context mContext;
    Query dataRef;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Category");
    SharedPreferences sharedpreferences;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle, txtDesc, txtSub;
        RelativeLayout rl;

        public MyViewHolder(View view) {
            super(view);
            txtTitle = (TextView) view.findViewById(R.id.txtTitleCat);
            txtDesc = (TextView) view.findViewById(R.id.txtDescriptionCat);
            txtSub = (TextView) view.findViewById(R.id.txtSubTitleCat);

            rl = (RelativeLayout) view.findViewById(R.id.relativeLayoutCategory);



/*            rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();
                    final Categories usrs = categoriesList.get(position);
                    String key = usrs.getKey();

                    Toast.makeText(mContext, "Clicked:  " + key, Toast.LENGTH_SHORT).show();

                }
            });*/


        }
    }


    public CategoryListAdapter(List<Categories> _categoriesList, Context context) {
        this.categoriesList = _categoriesList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Categories cat = categoriesList.get(position);
        //Log.e("TheFoodHub", "Fetched cat is: " + cat.getCategory());
        holder.txtTitle.setText(cat.getCategory());

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }


}
