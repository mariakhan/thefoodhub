package coms.example.arqam.foodhub;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amidtech on 12/03/2018.
 */

public class CartItem implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int i) {
            return new Item[i];
        }
    };
    String itemId;
    String itemName;
    String shopId;
    String itemImage;
    String itemDescription;
    String categoryId;
    double price;

    public CartItem(Parcel in) {
        itemId = in.readString();
        itemName = in.readString();
        shopId = in.readString();
        itemImage = in.readString();
        itemDescription = in.readString();
        categoryId = in.readString();
        price = in.readDouble();
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(itemId);
        parcel.writeString(itemName);
        parcel.writeString(shopId);
        parcel.writeString(itemImage);
        parcel.writeString(itemDescription);
        parcel.writeString(categoryId);
        parcel.writeDouble(price);
    }
}
