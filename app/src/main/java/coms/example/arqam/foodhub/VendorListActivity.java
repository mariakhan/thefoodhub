package coms.example.arqam.foodhub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class VendorListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    UsersListAdapter mAdapter;
    private List<Users> usersList = new ArrayList<>();
    FirebaseDatabase database;
    Query dataRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_list);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_vendors);
        GetAllVendors();
    }

    private void GetAllVendors() {


        database = FirebaseDatabase.getInstance();

        dataRef = database.getReference("Users").orderByChild("userType").equalTo("vendor");


        dataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                usersList.clear();
                ShowUsersData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void ShowUsersData(DataSnapshot dataSnapshot){

        for (DataSnapshot data : dataSnapshot.getChildren()){

            String mob = (String) data.child("mobile").getValue();
            String type = (String) data.child("userType").getValue();
            String isActive = (String) data.child("isActive").getValue();
            String firstName = (String) data.child("firstName").getValue();
            String lastName = (String) data.child("lastName").getValue();
            String address = (String) data.child("address").getValue();
            String key = (String) data.getKey();


            Users usr = new Users();
            usr.setKey(key);
            usr.setMobile(mob);
            usr.setIsActive(isActive);
            usr.setFirstName(firstName);
            usr.setLastName(lastName);
            usr.setAddress(address);

            usersList.add(usr);

        }

        //Show in Recycler View
        mAdapter = new UsersListAdapter(usersList, getApplicationContext());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }
}
