package coms.example.arqam.foodhub;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;


public class CartviewAdapter extends RecyclerView.Adapter<CartviewAdapter.MyViewHolder> {

    private List<Users> usersList;
    private Context mContext;
    Query dataRef;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemname, txtItemprice, txtQuantity,txtTotalprice;
        public Button btnRemove, btnOrder;

        public MyViewHolder(View view) {
            super(view);

            txtItemname = (TextView) view.findViewById(R.id.txtItemName);
            txtItemprice = (TextView) view.findViewById(R.id.txtItemPrice);
            txtQuantity = (TextView) view.findViewById(R.id.txtQuantity);
            txtTotalprice = (TextView) view.findViewById(R.id.txtTotalprice);

            btnRemove = (Button) view.findViewById(R.id.btnRemoveCart);
            btnOrder = (Button) view.findViewById(R.id.btnOrderCart);


            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();
                    final Users usrs = usersList.get(position);
                    myRef.child(usrs.getKey()).child("isActive").setValue("false");
                    Toast.makeText(mContext, "Blocked", Toast.LENGTH_SHORT).show();
                    try {

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            });

            btnOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /// Mark as done clicked
                    int position = getAdapterPosition();
                    final Users usrs = usersList.get(position);
                    myRef.child(usrs.getKey()).child("isActive").setValue("true");
                    Toast.makeText(mContext, "Activated", Toast.LENGTH_SHORT).show();
                    try {

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }


    public CartviewAdapter(List<Users> _usersList, Context context) {
        this.usersList = _usersList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Users usrs = usersList.get(position);
        holder.txtItemname.setText("Name: " + usrs.getFirstName() + " " + usrs.getLastName());
        holder.txtItemprice.setText("Address: " + usrs.getAddress());
        holder.txtQuantity.setText("Address: " + usrs.getAddress());
        holder.txtTotalprice.setText("Address: " + usrs.getAddress());



    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }


}
