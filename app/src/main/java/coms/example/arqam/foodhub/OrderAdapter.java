package coms.example.arqam.foodhub;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

/**
 * Created by Arqam on 3/14/2018.
 */

public class OrderAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txtorderid,txtorderstatus,txtorderphone,txtorderAddress;


    private AdapterView.OnItemClickListener itemClickListener;

    public OrderAdapter(View itemView) {
        super(itemView);
txtorderid=(TextView)itemView.findViewById(R.id.orderID);
txtorderAddress=(TextView)itemView.findViewById(R.id.orderAddress);
txtorderphone=(TextView)itemView.findViewById(R.id.orderPhone);
txtorderstatus=(TextView)itemView.findViewById(R.id.orderStatus);
itemView.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

    }
}
