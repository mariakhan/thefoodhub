package coms.example.arqam.foodhub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class AddEditCategoryActivity extends AppCompatActivity {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Category");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_category);


        final EditText txtCategort;
        Button btnSaveCategory;

        txtCategort = (EditText) findViewById(R.id.txt_cat_name);
        btnSaveCategory = (Button) findViewById(R.id.btn_save_cat);


        btnSaveCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Save Category
                //Check if category exists
                Query query = myRef.orderByChild("category")
                        .equalTo(txtCategort.getText().toString())
                        .limitToFirst(1);

                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getChildrenCount() > 0) {
                            Toast.makeText(getApplicationContext(), "Category already exist", Toast.LENGTH_LONG).show();
                        } else {
                            String catID = myRef.push().getKey();
                            myRef.child(catID).child("category").setValue(txtCategort.getText().toString());
                            Toast.makeText(getApplicationContext(), "Category created successfully", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w("TheFoodHub", "Failed to read value.", error.toException());
                    }


                });

            }
        });


    }
}
