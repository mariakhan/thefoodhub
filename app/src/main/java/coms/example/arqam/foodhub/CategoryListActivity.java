package coms.example.arqam.foodhub;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CategoryListActivity extends AppCompatActivity {

    FloatingActionButton fab;
    RecyclerView recyclerView;
    CategoryListAdapter mAdapter;
    private List<Categories> categoryList = new ArrayList<>();
    FirebaseDatabase database;
    Query dataRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_category);
        GetCategories();

        fab = (FloatingActionButton) findViewById(R.id.fab_add_cat);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cat = new Intent(CategoryListActivity.this, AddEditCategoryActivity.class);
                startActivity(cat);

            }
        });


    }

    private void GetCategories() {


        database = FirebaseDatabase.getInstance();

        dataRef = database.getReference("Category");


        dataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                categoryList.clear();
                ShowAllCategories(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void ShowAllCategories(DataSnapshot dataSnapshot) {

        for (DataSnapshot data : dataSnapshot.getChildren()) {

            String cat = (String) data.child("category").getValue();
            //Log.e("TheFoodHub","Cat is: " + cat);
            String key = (String) data.getKey();


            Categories category = new Categories();
            category.setKey(key);
            category.setCategory(cat);
            categoryList.add(category);

        }

        //Show in Recycler View
        mAdapter = new CategoryListAdapter(categoryList, getApplicationContext());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }
}
