package coms.example.arqam.foodhub.interfaces;

import coms.example.arqam.foodhub.Item;

/**
 * Created by amidtech on 11/03/2018.
 */

public interface ItemAddedListener {
    void itemAdded(Item item);
}
