package coms.example.arqam.foodhub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivitySendNotification extends AppCompatActivity {
    TextView txtSendNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notification);
        txtSendNotification = (TextView) findViewById(R.id.txtSendNotification);
    }
}
