package coms.example.arqam.foodhub;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class SplashActivity extends AppCompatActivity {
    private static final int SPLASH_DURATION = 7000; //7 seconds
    Context context;
    private boolean mIsBackButtonPressed;
    private Handler myhandler;


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        myhandler = new Handler();




    sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        Query query = myRef.orderByChild("mobile")
                .equalTo(sharedpreferences.getString(PreferenceKeys.PREFERENCE_USER_NUMBER, ""))
                .limitToFirst(1);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {

                        String pw = (String) data.child("password").getValue();

                        if (pw.equals(sharedpreferences.getString(PreferenceKeys.PREFERENCE_USER_PASSWORD, ""))) {
                          //  startActivity(new Intent(SplashActivity.this, MainActivity.class));


                            myhandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                    if (!mIsBackButtonPressed) {
                                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                        SplashActivity.this.startActivity(intent);
                                    }
                                }
                            }, SPLASH_DURATION);

                        } else {
                            Toast.makeText(getApplicationContext(), "Wrong password", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("TheFoodHub", "Failed to read value.", error.toException());
            }
        });
        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, SPLASH_DURATION);
    }
}
