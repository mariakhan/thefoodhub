package coms.example.arqam.foodhub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import com.google.firebase.storage.FirebaseStorage;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static coms.example.arqam.foodhub.PreferenceKeys.PREFERENCE_USER_ID;

public class CreateShopActivity extends Activity {
    private final static int IMAGE_REQUEST = 11;


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference shopTable = database.getReference("Shops");
    DatabaseReference userTable = database.getReference("Users");

    FirebaseStorage storage = FirebaseStorage.getInstance();

    Button Btn_CreateShop;
    EditText Edttxt_city, Edttxt_address, Edttxt_shopname, Edttxt_desc;
    Spinner spinner_categories;
    private ImageView img_add_shop_image;
    String userId;

    private Uri selectedImage;
    private FloatingActionButton uploadButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_create_shop);
        Edttxt_address = findViewById(R.id.txt_address);
        Edttxt_city = findViewById(R.id.txt_city);
        Edttxt_shopname = findViewById(R.id.txt_shopname);
        Edttxt_desc = findViewById(R.id.txt_shopdesc);
        img_add_shop_image = findViewById(R.id.imageView2);
        uploadButton= findViewById(R.id.btn_img_shop);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, IMAGE_REQUEST);
            }
        });


        userId = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE).getString(PREFERENCE_USER_ID, "");
        Btn_CreateShop = findViewById(R.id.btn_createshop);
        Btn_CreateShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(uploadButton.getDrawable()==null)
                {
                    Toast.makeText(getApplicationContext(), "upload image first", Toast.LENGTH_LONG).show();
                }
                else{//yahan se
                    Query query = shopTable.orderByChild("userId")
                            .equalTo(userId);

                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                Toast.makeText(getApplicationContext(), "Shop already exist", Toast.LENGTH_LONG).show();
                            } else {
                                new AsyncTask<Void, Void, String>() {
                                    private ProgressDialog dialog;
                                    private Uri downloadUrl;

                                    @Override
                                    protected void onPreExecute() {
                                        dialog = new ProgressDialog(CreateShopActivity.this);
                                        dialog.setMessage("Uploading");
                                        dialog.setCancelable(false);
                                        dialog.show();
                                    }

                                    @Override
                                    protected String doInBackground(Void... voids) {
                                        StorageReference fileRef = storage.getReference().child("shop-images" + UUID.randomUUID());
                                        if (selectedImage == null)
                                            return null;
                                        UploadTask uploadTask = fileRef.putFile(selectedImage);

                                        uploadTask.addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception exception) {
                                                // Handle unsuccessful uploads
                                                Log.d("TAG", "onFailure: " + exception);

                                            }
                                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                // This is the URL you are looking for, here you should insert it into your db instance...Something like this
                                                downloadUrl = taskSnapshot.getDownloadUrl();
                                                String shopId = shopTable.push().getKey();
                                                Shop shop = new Shop();
                                                shop.setShopAddress(Edttxt_address.getText().toString());
                                                shop.setShopCity(Edttxt_city.getText().toString());
                                                shop.setShopDescription(Edttxt_desc.getText().toString());
                                                shop.setShopTitle(Edttxt_shopname.getText().toString());
                                                shop.setUserId(userId);
                                                if(downloadUrl!= null){
                                                    shop.setShopLogo(downloadUrl.toString());
                                                }
                                                userTable.child(userId).child("userType").setValue("vendor");
                                                Toast.makeText(getApplicationContext(), "User change to Vendor", Toast.LENGTH_LONG).show();
                                                shopTable.child(shopId).setValue(shop);
                                            }
                                        });
                                        try {
                                            Tasks.await(uploadTask);
                                        } catch (ExecutionException e) {
                                            e.printStackTrace();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(String s) {
                                        super.onPostExecute(s);
                                        dialog.cancel();
                                        Toast.makeText(getApplicationContext(), "Shop created successfully", Toast.LENGTH_LONG).show();
                                        Intent main = new Intent(CreateShopActivity.this, VendorPanelActivity.class);
                                        startActivity(main);
                                    }
                                }.execute();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError error) {
                            // Failed to read value
                            Log.w("TheFoodHub", "Failed to read value.", error.toException());
                        }
                    });

                }   //yahan tuk
                }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            selectedImage = data.getData();
            uploadButton.setImageURI(selectedImage);
//            String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
            // Get the cursor
//            Cursor cursor = getContentResolver().query(selectedImage,
//                    filePathColumn, null, null, null);
//            cursor.moveToFirst();
//
//            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//            String imgDecodableString = cursor.getString(columnIndex);
//            cursor.close();
//            uploadButton.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
        }
    }

}