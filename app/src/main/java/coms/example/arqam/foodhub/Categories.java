package coms.example.arqam.foodhub;

/**
 * Created by XPS on 2/24/2018.
 */

public class Categories {

    String key;
    String category;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
