package coms.example.arqam.foodhub;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.MyViewHolder> {


    Query dataRef;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Item");
    private List<Item> itemList;
    private Context mContext;

    public ItemListAdapter(List<Item> _itemList, Context context) {
        notifyDataSetChanged();
        this.itemList = _itemList;
        this.mContext = context;
    }

    public void setList(List<Item> items) {
        notifyDataSetChanged();
        this.itemList = items;
        notifyDataSetChanged();
    }

    @Override
    public ItemListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemview, parent, false);
        return new ItemListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ItemListAdapter.MyViewHolder holder, int position) {
        final Item itm = itemList.get(position);
        holder.bind(itm);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgitm;
        private TextView txtItemName, txtItemDesc, txtItemVender, txtItemPrice;
        private LinearLayout itemContainer;

        public MyViewHolder(View view) {
            super(view);


            txtItemName = (TextView) view.findViewById(R.id.txtItemName);
            txtItemDesc = (TextView) view.findViewById(R.id.txtItemDescription);
            txtItemPrice = (TextView) view.findViewById(R.id.txtItemPrice);
            imgitm = (ImageView) view.findViewById(R.id.img_Item_Image);
            itemContainer = view.findViewById(R.id.LinearLayout);


        }

        public void bind(final Item itm) {
            txtItemName.setText("Name: " + itm.getItemName());
            txtItemDesc.setText("Description: " + itm.getItemDescription());
            txtItemPrice.setText("Price: " + itm.getPrice());
            // Picasso.with(mContext).load(itm.getItemImage()).into(imgitm);
            Picasso.with(mContext).load(itm.getItemImage()).fit().centerCrop()
                    .into(imgitm);


            itemContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ((MainActivity) mContext).itemAdded(itm);
                    return true;
                }
            });
        }

    }


}