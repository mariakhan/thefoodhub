package coms.example.arqam.foodhub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import static coms.example.arqam.foodhub.PreferenceKeys.PREFERENCE_USER_ID;

public class Signup extends Activity {
    Button signup;
    EditText mob, pass, confirm_pass, txtFirstName, txtLastName, txtAddress;
    boolean exist = false;

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");

    DatabaseReference userExist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_signup);


        signup = findViewById(R.id.btnsignup);


        mob = findViewById(R.id.txt_mob_no);
        mob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mob.getText().toString().length() < 11) {
                    mob.setError("Please input 11 digit mobile number");
                } else {
                    mob.setError(null);
                }
            }
        });

        pass = findViewById(R.id.txt_pass);
        pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (pass.getText().toString().length() <= 11) {
                    pass.setError("password must have 7 character");
                } else {
                    pass.setError(null);
                }
            }
        });
        confirm_pass = findViewById(R.id.txt_confrim_pass);

        txtFirstName = findViewById(R.id.txt_first_name);

        txtLastName = findViewById(R.id.txt_last_name);
        txtAddress = findViewById(R.id.txt_address);

        sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);


        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Sign up user
                if (pass.getText().toString().equals(confirm_pass.getText().toString())) {

                    //Check if user exists
                    Query query = myRef.orderByChild("mobile")
                            .equalTo(mob.getText().toString())
                            .limitToFirst(1);

                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if (dataSnapshot.getChildrenCount() > 0) {
                                Toast.makeText(getApplicationContext(), "Accout already exist", Toast.LENGTH_LONG).show();
                            } else {
                                String userId = myRef.push().getKey();
                                Users usr = new Users();
                                usr.setMobile(mob.getText().toString());
                                usr.setPassword(pass.getText().toString());
                                usr.setFirstName(txtFirstName.getText().toString());
                                usr.setLastName(txtLastName.getText().toString());
                                usr.setAddress(txtAddress.getText().toString());
                                myRef.child(userId).setValue(usr);
                                Toast.makeText(getApplicationContext(), "Accout created successfully", Toast.LENGTH_LONG).show();

                                //Save user id in prefrences
                                editor = sharedpreferences.edit();
                                editor.putString("uid", userId);
                                editor.putString(PreferenceKeys.PREFERENCE_USER_NUMBER, mob.getText().toString().trim());
                                editor.putString(PreferenceKeys.PREFERENCE_USER_PASSWORD, pass.getText().toString().trim());
                                editor.putString("type", "user");
                                editor.putString(PREFERENCE_USER_ID, userId);
                                editor.apply();

                                //Show main form
                                Intent main = new Intent(Signup.this, MainActivity.class);
                                startActivity(main);
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError error) {
                            // Failed to read value
                            Log.w("TheFoodHub", "Failed to read value.", error.toException());
                        }


                    });
                } else {
                    Snackbar.make(v, "Password does not match, please enter the same password", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });


    }
}
