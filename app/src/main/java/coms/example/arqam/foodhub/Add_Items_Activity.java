package coms.example.arqam.foodhub;



        import android.app.Activity;
        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.database.Cursor;
        import android.graphics.BitmapFactory;
        import android.net.Uri;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.provider.MediaStore;
        import android.support.annotation.NonNull;
        import android.support.v7.app.AppCompatActivity;
        import android.text.Editable;
        import android.text.TextWatcher;
        import android.util.Log;
        import android.view.View;
        import android.view.Window;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.Spinner;

        import com.google.android.gms.tasks.OnFailureListener;
        import com.google.android.gms.tasks.OnSuccessListener;
        import com.google.android.gms.tasks.Tasks;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.Query;
        import com.google.firebase.database.ValueEventListener;
        import com.google.firebase.storage.FirebaseStorage;
        import com.google.firebase.storage.StorageReference;
        import com.google.firebase.storage.UploadTask;

        import java.util.UUID;
        import java.util.concurrent.ExecutionException;

        import static coms.example.arqam.foodhub.PreferenceKeys.PREFERENCE_USER_ID;

public class Add_Items_Activity extends Activity {

    // int REQUEST_CODE = 1;
    private final static int IMAGE_REQUEST = 11;
    Button Btn_add_items;
    EditText Edittext_itemname, Edittext_itemdesc, Edittext_itemprice;
    Spinner spinner_categories;
    ImageView img_add_item_image;

    FirebaseStorage storage = FirebaseStorage.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference itemTable = database.getReference("Item");
    DatabaseReference userTable = database.getReference("Users").child("userId");
    DatabaseReference shopTable = database.getReference("Shops");
    private String userId;
    private ImageView uploadButton;
    private String imageUri;
    private Uri selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_add__items_);
        userId = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE).getString(PREFERENCE_USER_ID, "");
        uploadButton = findViewById(R.id.btn_img);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, IMAGE_REQUEST);
            }
        });
        Edittext_itemname = findViewById(R.id.txt_add_item_name);
        Edittext_itemdesc = findViewById(R.id.txt_add_item_desc);
        Edittext_itemdesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (Edittext_itemdesc.getText().toString().length() <= 50) {
                    Edittext_itemdesc.setError("item desc length below 50");
                } else {
                    Edittext_itemdesc.setError(null);
                }
            }
        });
        Edittext_itemprice = findViewById(R.id.txt_add_item_price);

        Btn_add_items = findViewById(R.id.btnAddItemofShop);

        img_add_item_image = findViewById(R.id.imageView2);


        spinner_categories = findViewById(R.id.spinner_categ);

        final String[] items = new String[]{"chines", "Desi", "BBQ", "Grilled", "Desert"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner_categories.setAdapter(adapter);


        Btn_add_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = shopTable.orderByChild("userId")
                        .equalTo(userId);

                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {

                        new AsyncTask<Void, Void, String>() {
                            private ProgressDialog dialog;
                            private Uri downloadUrl;

                            @Override
                            protected void onPreExecute() {
                                dialog = new ProgressDialog(Add_Items_Activity.this);
                                dialog.setMessage("Uploading");
                                dialog.setCancelable(false);
                                dialog.show();
                            }

                            @Override
                            protected String doInBackground(Void... voids) {
                                StorageReference fileRef = storage.getReference().child("images"+ UUID.randomUUID());
                                if (selectedImage == null)
                                    return null;
                                UploadTask uploadTask = fileRef.putFile(selectedImage);

                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle unsuccessful uploads
                                        Log.d("TAG", "onFailure: " + exception);

                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        // This is the URL you are looking for, here you should insert it into your db instance...Something like this
                                        downloadUrl = taskSnapshot.getDownloadUrl();

                                        String shopId = dataSnapshot.getChildren().iterator().next().getKey();
                                        String itemId = itemTable.push().getKey();
                                        final Item item = new Item();
                                        item.setItemName(Edittext_itemname.getText().toString());
                                        item.setItemDescription(Edittext_itemdesc.getText().toString());
                                        item.setPrice(Double.parseDouble(Edittext_itemprice.getText().toString()));
                                        item.setShopId(shopId);
                                        if (downloadUrl != null) {
                                            item.setItemImage(downloadUrl.toString());
                                        }
                                        itemTable.child(itemId).setValue(item);
                                    }
                                });
                                try {
                                    Tasks.await(uploadTask);
                                    if (downloadUrl != null)
                                        return downloadUrl.toString();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(String imagePath) {
                                dialog.cancel();
                                Intent main = new Intent(Add_Items_Activity.this, ShopProfileActivity.class);
                                startActivity(main);
                            }
                        }.execute();
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w("TheFoodHub", "Failed to read value.", error.toException());
                    }


                });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            selectedImage = data.getData();
            uploadButton.setImageURI(selectedImage);
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imgDecodableString = cursor.getString(columnIndex);
            cursor.close();
            uploadButton.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

        }
    }
}