package coms.example.arqam.foodhub;

public interface PreferenceKeys {
    static final String PREFERENCE_USER_ID = "coms.example.arqam.foodhub.userId";
    static final String PREFERENCE_USER_PASSWORD = "coms.example.arqam.foodhub.password";
    static final String PREFERENCE_USER_NUMBER = "coms.example.arqam.foodhub.phone.number";

    static final String PREFERENCE_SHOP_ID = "coms.example.arqam.foodhub.shopId";
  //  static final String PREFERENCE_SHOP_ID = "pk.fastech.thefoodhub.thefoodhubv2.shopId";
}
