package coms.example.arqam.foodhub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ShopProfileActivity extends Activity {

    RecyclerView recyclerView;
    ItemListAdapter mAdapter;
    private List<Item> itemList = new ArrayList<>();
    FirebaseDatabase database;
    private Context mContext;
    //Query datashop = database.getReference("Shops");
    Query dataRef;
    TextView Rshopname;
    TextView Rshopdesc;
    ImageView img;
    DatabaseReference myRef;
    String userId;
    String userkey;

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    Shop shop = new Shop();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_shop_profile);

        sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        Rshopname = findViewById(R.id.txt_profile_shop_shopname);
        Rshopdesc = findViewById(R.id.txt_profile_shop_desc);
        img = findViewById(R.id.imageView2);
        Picasso.with(mContext).load(R.drawable.shopimgs).fit().centerCrop().into((ImageView) findViewById(R.id.imageView2));
        FloatingActionButton fab = findViewById(R.id.add_item_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ShopProfileActivity.this, Add_Items_Activity.class));
            }
        });
        recyclerView = findViewById(R.id.recycler_view_items);
        mAdapter = new ItemListAdapter(itemList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        String userId = sharedpreferences.getString(PreferenceKeys.PREFERENCE_USER_ID, "");
        myRef = FirebaseDatabase.getInstance().getReference().child("Shops");
        Query query = myRef.orderByChild("userId").equalTo(userId);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Shop shop = dataSnapshot.getChildren().iterator().next().getValue(Shop.class);
                String shoptitle = shop.getShopTitle();
                sharedpreferences.edit().putString(PreferenceKeys.PREFERENCE_SHOP_ID, shop.getShopId()).apply();
                Rshopname.setText(shoptitle);
                Rshopdesc.setText(shop.getShopDescription());

                Query query1 = FirebaseDatabase.getInstance().getReference()
                        .child("Items").orderByChild("shopId").equalTo(shop.getShopId());
                query1.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ShowItemsData(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        GetAllItems();
    }

    private void GetAllItems() {


        database = FirebaseDatabase.getInstance();

        dataRef = database.getReference("Item");



        dataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                itemList.clear();
                ShowItemsData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void ShowItemsData(DataSnapshot dataSnapshot) {
        

        for (DataSnapshot data : dataSnapshot.getChildren()) {


            Item itm = data.getValue(Item.class);

            itemList.add(itm);



        }

        //Show in Recycler View

        mAdapter.setList(itemList);


    }
}