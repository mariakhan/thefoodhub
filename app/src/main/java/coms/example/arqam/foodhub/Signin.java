package coms.example.arqam.foodhub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class Signin extends Activity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signin);


        final EditText txtUsername, txtpassword;
        Button btnSignin;

        txtUsername = findViewById(R.id.txt_email);

        txtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txtUsername.getText().toString().length() <= 11) {
                    txtUsername.setError("Please input 11 digit mobile number");
                } else {
                    txtUsername.setError(null);
                }
            }
        });

        txtpassword = findViewById(R.id.txt_pass);
        txtpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txtpassword.getText().toString().length() <= 7) {
                    txtpassword.setError("password must be 7 characters");
                } else {
                    txtpassword.setError(null);
                }
            }
        });

        btnSignin = findViewById(R.id.btn_signin);

        sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Authenticate user
                //Check if user exists

                userId = txtUsername.getText().toString();

                Query query = myRef.orderByChild("mobile")
                        .equalTo(txtUsername.getText().toString())
                        .limitToFirst(1);

                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        if (dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot data : dataSnapshot.getChildren()) {

                                String key = data.getKey();
                                String pw = (String) data.child("password").getValue();
                                String isActive = (String) data.child("isActive").getValue();
                                String type = (String) data.child("userType").getValue();


                                if (pw.equals(txtpassword.getText().toString())) {
                                    if (isActive.equals("true")) {

                                        //Save user id in prefrences
                                        editor = sharedpreferences.edit();
                                        editor.putString(PreferenceKeys.PREFERENCE_USER_PASSWORD, txtpassword.getText().toString());
                                        editor.putString(PreferenceKeys.PREFERENCE_USER_NUMBER, txtUsername.getText().toString());
                                        editor.putString(PreferenceKeys.PREFERENCE_USER_ID, key);
                                        editor.putString("uid", key);
                                        editor.putString("type", type);
                                        editor.apply();

                                        if (type.equals("admin")) { //Show Admin portal
                                            Intent main = new Intent(Signin.this, AdminPanelActivity.class);
                                            startActivity(main);
                                        } else if (type.equals("vendor")) {

                                            Intent main = new Intent(Signin.this, VendorPanelActivity.class);
                                            startActivity(main);
                                        } else { //Show user  portal
                                            Intent main = new Intent(Signin.this, MainActivity.class);
                                            startActivity(main);
                                        }

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Your account has been banned", Toast.LENGTH_SHORT).show();
                                    }

                                } else {
                                    Toast.makeText(getApplicationContext(), "Wrong password", Toast.LENGTH_SHORT).show();

                                }


                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No accout exist, please create an account", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w("TheFoodHub", "Failed to read value.", error.toException());
                    }
                });

            }
        });


    }
}
